# qemu-toolkit(7) -- A toolkit for running qemu on Illumos / OmniOS

## SYNOPSIS

qemu-toolkit is a small set of scripts to control QEMU kvm virtualised
machines on a set of illumos hosts. Have a look at 'storadm' and 
'vmadm' - inline help will show you the possibilities.

## INSTALLATION

qemu-toolkit is a Ruby gem. This means you can install it with one simple
command: 

    gem install qemu-toolkit
    
For full installation instructions, please see qemu-toolkit-install(7). 

## CONTRIBUTE

This project is hosted on
[bitbucket](https://bitbucket.org/kschiess/qemu-toolkit). An issue tracker
is available at
[bitbucket](https://bitbucket.org/kschiess/qemu-toolkit/issues).

We value all contributions, provided they

 * are tested and have a clear rationale
 * are generally useful
 * come as a clean changeset (pull request or email patch)

Discussion regarding this project via 'qemutoolkit' mailing list. Subscribe by
sending any kind of message to this
[address](mailto:qemutoolkit@librelist.com).

## SEE ALSO

qemu-toolkit-overview(7), qemu-toolkit-install(7),
qemu-toolkit-configuration(7), storadm(1), vmadm(1), README(7)

## AUTHORS

Copyright (c) 2012 Kaspar Schiess. 

## COPYRIGHT

This tool is under a MIT license. Please see the LICENSE file in the original
source.