
# vmadm(1) -- manages qemu-toolkit virtual machines

## SYNOPSIS

`vmadm` [-h]<br>
`vmadm list`<br>
`vmadm create` <name><br>
`vmadm start` [--bootiso <iso_file>]<name><br>
`vmadm shutdown` <name><br>
`vmadm kill` <name><br>
`vmadm monitor` <name><br>
`vmadm vnc` <name><br>
`vmadm console` <name><br>

## DESCRIPTION

The `vmadm` command manages virtual machines. A virtual machine is defined 
by using the `virtual_machine` syntax in a file below `/etc/qemu-toolkit`. 
(see qemu-toolkit-configuration(7)) 

  * `vmadm list`:
    Lists all virtual machines that are defined on this system. 
  
  * `vmadm create` <name>: 
    Creates a new virtual machine definition and fills it with sensible default
    values. 
  
  * `vmadm start` <name>: 
    Starts a virtual machine using QEMU/KVM. 
    
    * `--bootiso` <iso_file>:
      Boots the virtual machine once from virtual cdrom backed by the iso file
      given. Upon reboot, the machine will use its first disk to boot. 
    
  * `vmadm shutdown` <name>: 
    Sends the virtual machine a system/powerdown event. 
    
  * `vmadm kill` <name>: 
    Immediately kills the virtual machine.
    
  * `vmadm monitor` <name>: 
    Enters an interactive monitor mode that controls the virtual machine. The
    machine must be running for this to work. Use Control+D to exit. 
    
  * `vmadm vnc` <name>: 
    Connect the terminal to a VNC server that displays the console of the 
    virtual machine. 
    
  * `vmadm console` <name>: 
    Connects the terminal to the serial console of the virtual machine. Note
    that this must be specially configured for most guest OSes.
    
## EXAMPLES

### Example 1 Boot a virtual machine
The following command starts the virtual machine and boots it from its 
first disk. 

    # vmadm start my_vm
    
### Example 2 Install a virtual machine from ISO
The following command boots the virtual machine from a virtual cdrom. The
cdrom is backed by an ISO file called ubuntu.iso. 

    # vmadm start my_vm --bootiso ubuntu.iso

## BUGS

When using iscsi targets, you have to relaunch the vm twice to get it to 
start up. We have code in place to work around this, but it is not effective. 
Work to fix this is underway. 

## SEE ALSO

qemu-toolkit-overview(7), qemu-toolkit-install(7),
qemu-toolkit-configuration(7), storadm(1), vmadm(1), README(7)

## AUTHORS

Copyright (c) 2012 Kaspar Schiess. 

## COPYRIGHT

This tool is under a MIT license. Please see the LICENSE file in the original
source.