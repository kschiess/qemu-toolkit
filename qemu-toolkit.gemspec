# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = 'qemu-toolkit'
  s.version = '0.5.2'

  s.authors = ['Kaspar Schiess']
  s.email = 'kaspar.schiess@technologyastronauts.ch'

  s.homepage = 'http://kschiess.bitbucket.org/qemu-toolkit/README.7.html'
  s.license = 'MIT'
  s.summary = %Q(Manages QEMU kvm virtual machines on Illumos hosts.)
  s.description = %Q(
    qemu-toolkit is a collection of small tools that help in managing and
    running QEMU virtual machines on Illumos/OmniOS. It supports both
    local operation and SAN-style block storage via iSCSI. 
  )

  s.extra_rdoc_files = ['README.md']
  s.files = %w(LICENSE README.md) + Dir.glob("{lib,bin,doc}/**/*")
  s.executables = %w(vmadm storadm)
  s.rdoc_options = ['--main', 'README.md']
  s.require_paths = ['lib']

  s.required_ruby_version = '>= 1.9.3'
  
  s.add_dependency 'clamp', '~> 0.5'
end
