module QemuToolkit::Network
  class MacAddress
    def initialize address
      @address = normalize(address)
    end

    def normalize str
      elements = str.split(':')

      fail "Malformed MAC address: not enough or too many elements (should == 6, was #{elements.size})." \
        unless elements.size == 6

      elements.map { |n|
        n.downcase!
        case n.size
          when 1
            '0' + n
          when 2
            n
        else
          fail "Malformed MAC address: #{str}; should have 6 elements with max. 2 digits."
        end
      }.join(':')
    end

    def == other
      self.to_s == other.to_s
    end

    def to_s
      @address
    end
  end
end