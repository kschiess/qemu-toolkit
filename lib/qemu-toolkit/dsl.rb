
module QemuToolkit
  # A generic dsl class. You define a target keyword you want to associate with
  # an object instance you also give during construction. The dsl will then 
  # react only to a call to TARGET, allowing to use a block to configure the 
  # object you give. 
  #
  # Inside the block, the following delegations are made: 
  #
  #   foo 'some value'
  #   # delegated to obj.foo= 'some value' if possible
  #   # or then to obj.add_foo 'some_value'
  #
  # @example
  #   class Bar
  #     attr_accessor :name
  #     attr_accessor :test
  #   end
  #   bar = Bar.new
  #
  #   dsl = QemuToolkit::DSL::File.new
  #   dsl.add_toplevel_target :foo, { |name| Bar.new(name) }
  #   dsl.load_file(some_file)
  #
  #   # In this example, some_file would contain something like
  #   foo('name') do
  #     test 'something'
  #   end
  #   bar.name # => 'name'
  #   bar.test # => 'something'
  #
  class DSL
    class File
      attr_reader :objects
      
      def initialize
        @objects = []
      end
      def load_file(path)
        eval(
          ::File.read(path), 
          binding, 
          path)
      end
      def require file
        Kernel.require file
      end
      def add_toplevel_target target, producer
        define_singleton_method(target) { |*args, &block| 
          object = producer.call(*args) 
          Unit.new(object, &block)
          
          @objects << object
        }
      end
    end
    class Unit
      def initialize(obj, &block)
        @object = obj
        instance_eval(&block)
      end
      
      def method_missing(sym, *args, &block)
        delegate = find_delegate_method(sym, @object)
        return super unless delegate

        if delegate.arity == args.size
          delegate.call(*args, &block)
        else
          delegate.call(args, &block)
        end
      end
      def respond_to?(sym)
        find_delegate_method(sym, @object) || super
      end

    private 
      def find_delegate_method sym, obj
        ["#{sym}=", "add_#{sym}"].each { |dm| 
          return obj.method(dm) if obj.respond_to? dm } 
        nil
      end
    end
  end
end