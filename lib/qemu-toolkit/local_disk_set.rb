module QemuToolkit

  class LocalDiskSet
    def self.for(name, backend)
      output = backend.zfs :list, "-oname,#{QemuToolkit::EXPORT_TAG} -H"
      candidates = output.lines.map { |l| l.chomp.strip.split }
      candidates.reject! { |n,_| ! n }
      
      # Finds all qemu-toolkit storage spaces that end in '/name':
      # Output from this step are only the spaces that match
      storage_spaces = candidates.
        select { |cand_name, flag| 
          %w(true false).include?(flag) && cand_name.end_with?('/'+name) }

      # Finds all disks for each of the candidate exports: 
      # Output from this step should be <name, disks> tuples
      storage_spaces.
        map { |base_name, _| new(
          base_name, 
          candidates.map(&:first).
            select { |name| 
              
              name.match(/#{Regexp.escape(base_name)}\/disk\d+/) } ) }
    end
    
    def initialize(name, disks)
      @name = name
      @disks = disks
    end
    
    attr_reader :name
    
    def each_disk
      @disks.sort.each do |path|
        yield "/dev/zvol/rdsk/#{path}"
      end
    end
  end
end
