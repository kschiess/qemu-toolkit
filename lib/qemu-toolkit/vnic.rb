
require 'set'

require 'qemu-toolkit/network/mac_address'

module QemuToolkit
  class Vnic
    class << self
      def for_prefix(prefix, background)
        vnics = VnicCollection.new
        links = background.dladm 'show-vnic', '-po link,over,vid,macaddress'

        re_separator = /(?<!\\):/
        
        links.each_line do |line|
          next unless line.start_with?(prefix)
          link, over, vid, macaddr = line.chomp.split(re_separator)

          # macaddr being the only place escaping takes place, unescape it
          # by replacing \: -> :
          macaddr.gsub!(/\\:/, ':')
          
          # Assumes that vid 0 is always the 'no vlan' VLAN
          over = "#{over}:#{vid}" if vid.to_i > 0
          
          if md=link.match(/^(?<vm>.*)_(?<link_no>\d+)$/)
            vnics << Vnic.new(md[:vm], Integer(md[:link_no]), over, macaddr)
          end
        end
        
        vnics
      end
      
      def create(prefix, over, backend, macaddr=nil)
        # Retrieve links that exist for this prefix and this over interface
        vnics = for_prefix(prefix, backend)
        next_vnic_number = (vnics.map(&:number).max || 0) + 1

        new(prefix, next_vnic_number, over, macaddr).tap { |o|
          o.create(backend) }
      end
      
    end
    
    def initialize(prefix, number, over, macaddr=nil)
      @prefix, @number, @over = prefix, number, over
      @macaddr = macaddr && Network::MacAddress.new(macaddr)

      if vlan_tagged?
        @interface, @vlan = @over.split(':')
      else
        @interface = @over
        @vlan = nil
      end
    end
    
    attr_reader :prefix
    attr_reader :number
    attr_reader :over
    attr_reader :macaddr

    attr_reader :interface
    attr_reader :vlan
    
    def ==(other)
      self.prefix == other.prefix && 
        self.number == other.number && 
        self.over == other.over && 
        self.macaddr == other.macaddr
    end

    def to_s
      "#{vnic_name} (#{vlan || 'g'}/#{macaddr})"
    end

    def vlan_tagged?
      @over.index(':')
    end
    def mac_address?
      @macaddr
    end
    
    def create backend
      opts = ["-l #{interface}"]

      opts << "-v #{vlan}" if vlan_tagged?
      opts << "--mac-address #{macaddr}" if mac_address?

      backend.dladm 'create-vnic', *opts, vnic_name
    end
    
    def vnic_name
      "#{prefix}_#{number}"
    end
  end
end