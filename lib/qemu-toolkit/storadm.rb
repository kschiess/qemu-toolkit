require 'clamp'

require 'qemu-toolkit'

module QemuToolkit
  class Storadm < Clamp::Command 
    option ['-v', '--verbose'], :flag, 'be chatty'
    option ['-n', '--dry-run'], :flag, "don't execute commands, instead just print them"
    
    # Command backend to use during the processing of subcommands. 
    #
    def backend
      Config.backend
    end
    
    # A factory method for VM storage. 
    #
    def storage(name)
      VMStorage.new(name, backend)
    end
    
    # Main execute method - delegates to _execute in the subcommands. This 
    # handles transforming Ruby errors into simple shell errors. 
    #
    def execute
      backend.verbose = verbose?

      _execute
    rescue => error
      raise if verbose? || $rspec_executing

      $stderr.puts error.to_s
      exit 1
    end
        
    subcommand('clone', 
      'Clones an existing VM storage as starting point for quickly creating new VMs.') do
        
        parameter 'NAME', 'name of the new VM storage, ie: new_vm'
        parameter 'TEMPLATE', 'VM storage to use as a template, ie: pool2/template'
        parameter 'VERSION', 'template version to clone (aka zfs snapshot), ie: v1.2'
        
        def _execute
          storage(template).clone(name, version)
        end
      end
    
    subcommand('export', 
      'Creates an iSCSI target for the VM storage, with each disk mapped to a LUN.') do
        parameter 'NAME', 'name of the VM storage, ie: pool1/new_vm'
        
        def _execute
          s = storage(name)
          # Export the storage
          s.export
          # Print the IQN to be helpful. 
          puts "Created: #{s.iqn}"
        end
      end
    
    subcommand('hide', 
      'Removes iSCSI target for the VM storage from the system.') do
        parameter 'NAME', 'name of the VM storage, ie: pool1/new_vm'

        def _execute
          storage(name).hide
        end
      end
    
    subcommand('create', 
      "Creates a VM storage space from scratch. Pass the size for at least one disk volume.") do
        
        parameter 'NAME', 'name of the VM storage, ie: pool1/new_vm'
        parameter 'SIZE ...', 'sizes of one or more disks, in ZFS format, ie: 10G'
        
        def _execute
          fail "Must create at least one disk." if size_list.empty?
          
          s = storage(name)

          fail "Must specify absulute storage path (pool/dataset) for creation." \
            if s.relative_name?
              
          s.create(size_list)
        end
      end

    subcommand('list', 
      "Lists all vm stores on this machine.") do
        
        def _execute
          re_splitter = /\s+/
          store_list = backend.zfs :list, "-H -oname,#{QemuToolkit::EXPORT_TAG} -t filesystem"
          store_list.each_line do |line|
            clean_line = line.chomp.strip
            last_space = clean_line.rindex(re_splitter)
            next unless last_space
          
            name, _, flag = clean_line.rpartition(re_splitter)
            if flag != '-'
              printf "%-20s", name
              puts (flag == 'true') ? 
                storage(name).iqn :
                '-'
            end
          end
        end
      end
  end
end