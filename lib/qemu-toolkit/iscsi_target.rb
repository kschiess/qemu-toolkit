module QemuToolkit
  class ISCSITarget
    def initialize(iqn, address, backend)
      @iqn, @address = iqn, address
      @backend = backend
      
      # Keep track if disks has ever returned an array of size > 0. This would
      # mean that the target is connected and will stay connected until we 
      # change that. 
      @mapped = false
    end
    
    attr_reader :iqn
    attr_reader :address
    
    def mapped?
      disks.size > 0
    end
    
    def ensure_exists
      # If the target is mapped already, nothing to do. 
      return if mapped?

      # Map the target
      begin
        @backend.iscsiadm :add, 'static-config', "#{iqn},#{address}", '2>/dev/null'
      rescue 
        # Ignore already mapped targets
      end
      
      print "Waiting for iSCSI disks to come online..."
      while !mapped?
        print '.'
        sleep 0.5 # takes a short while until login
      end
      puts 'OK.'
    end
    
    # A cached 'list target -vS'
    #
    def target_list
      @backend.iscsiadm :list, :target, '-vS'
    end
    
    def disks
      luns = []
      
      state = 0
      last_lun = nil
            
      target_list.each_line do |line|
        case state
          when 0
            state += 1 if line.match(/^Target: #{Regexp.escape(iqn)}\n/m)
            lun = nil
          when 1
            if md=line.match(/^\s+LUN: (\d+)\n/m)
              last_lun = md[1] 
            end
            
            if last_lun && md=line.match(/^\s+OS Device Name: (\/dev\/rdsk\/.*)\n/m)
              luns << [Integer(last_lun), md[1]]
            end
            
            state += 1 if line.match(/^Target: /)
        end
      end
      
      luns.sort_by { |no,_| no }.map { |_, dev| p0ify(dev) }
    end

  private
    def p0ify(str)
      # /dev/rdsk/c4t600144F0503CC9000000503F8B09000Ad0s2 to 
      # /dev/rdsk/c4t600144F0503CC9000000503F8B09000Ad0p0
      
      str.sub(/s2$/, 'p0')
    end
  end
end