
module QemuToolkit
  EXPORT_TAG = 'qemu_toolkit:export'
  
  module Backend; end
end

require 'qemu-toolkit/config'
require 'qemu-toolkit/vm'
require 'qemu-toolkit/vm_storage'
require 'qemu-toolkit/backend/illumos'
require 'qemu-toolkit/vnic'
require 'qemu-toolkit/vnic_collection'