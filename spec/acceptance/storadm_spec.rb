require 'spec_helper'

require 'qemu-toolkit'
require 'qemu-toolkit/storadm'

require 'support/backend_fake'

describe "storadm" do
  let(:backend) { BackendFake.new(
    /stmfadm create-lu/ => 'Logical unit created: 1234567890987654321', 
    /zfs get -Ho value #{QemuToolkit::EXPORT_TAG} pool\/foobar/ => "false\n",
    /stmfadm list-lu/ => %Q(
      LU Name: 600144F0503CC9000000503E47BF0004
          Data File         : /dev/zvol/rdsk/b1/test1/disk1
      LU Name: 600144F0503CC9000000503E47BF0006
          Data File         : /dev/zvol/rdsk/b1/test1/disk2
      LU Name: 600144F0503CC9000000503E48550005
          Data File         : /dev/zvol/rdsk/b1/test2/disk1
    )) }

    def invoke(*given_args)
      flexmock(QemuToolkit::Config.current, backend: backend)
      
      storadm = QemuToolkit::Storadm.new('storadm', context={})
      storadm.run given_args
    end
    def stdout(*args)
      capture(:stdout) {
        invoke(*args)
      }
    end
  
  # zfs list -o name,qemu-toolkit:export -t filesystem
  describe 'list' do
    before(:each) do
      backend.mapping.store(/#{QemuToolkit::EXPORT_TAG}/, %Q(
      b1	-
      b1/dns1	true
      b1/dns2	true
      b1/fw1	true
      b1/lan1	true
      b1/smith	true
      rpool -
))
    end
    it "lists all vm stores" do
      output = stdout("list")
      
      output.should include("b1/dns1             iqn.2012-01.com.qemu-toolkit:dns1")
      output.should include "b1/dns2             iqn.2012-01.com.qemu-toolkit:dns2"
    end
  end
  describe 'export NAME' do
    it "exports an iscsi target for the given dataset" do
      invoke "export", 'pool/foobar'
      
      backend.commands.should =~ [
        "zfs get -Ho value #{QemuToolkit::EXPORT_TAG} pool/foobar",
        'stmfadm create-tg foobar', 
        'stmfadm create-lu /dev/zvol/rdsk/pool/foobar/disk1', 
        'stmfadm add-view -t foobar 1234567890987654321', 
        'stmfadm create-lu /dev/zvol/rdsk/pool/foobar/disk2', 
        'stmfadm add-view -t foobar 1234567890987654321', 
        'stmfadm add-tg-member -g foobar iqn.2012-01.com.qemu-toolkit:foobar', 
        'itadm create-target -n iqn.2012-01.com.qemu-toolkit:foobar -t frontend', 
        "zfs set #{QemuToolkit::EXPORT_TAG}=true pool/foobar" ]
    end 
  end
  describe 'hide NAME' do
    it "removes all iscsi export constructs" do
      backend.mapping.store %r(^zfs get.*), "true\n"
      
      invoke 'hide', 'test1'
      
      backend.commands.should =~ [
        "zfs get -Ho value #{QemuToolkit::EXPORT_TAG} test1",
        'stmfadm offline-target iqn.2012-01.com.qemu-toolkit:test1', 
        'itadm delete-target iqn.2012-01.com.qemu-toolkit:test1', 
        'stmfadm list-lu -v',
        'stmfadm delete-lu 600144F0503CC9000000503E47BF0004', 
        'stmfadm delete-lu 600144F0503CC9000000503E47BF0006', 
        'stmfadm delete-tg test1', 
        "zfs set #{QemuToolkit::EXPORT_TAG}=false test1"]
    end 
    it "refuses work if there is no such exported ds" do
      backend.mapping.store %r(zfs get -Ho value #{QemuToolkit::EXPORT_TAG}), 
        'false'
      
      expect {
        invoke 'hide', 'test1'
      }.to raise_error
    end 
  end
  describe 'clone NAME TEMPLATE VERSION' do
    it "clones the template to the machine, exporting iscsi targets" do
      invoke 'clone', 'test', 'b1/template', 'v1-0-0'
      
      backend.commands.should == [
        'zfs clone b1/template@v1-0-0 b1/test', 
        'zfs clone b1/template/disk1@v1-0-0 b1/test/disk1', 
        'zfs clone b1/template/disk2@v1-0-0 b1/test/disk2', 
        "zfs set #{QemuToolkit::EXPORT_TAG}=false b1/test"]
    end 
  end
  describe 'create NAME SIZE1 SIZE2 SIZE3' do
    it "creates an empty machine storage with the given disks" do
      invoke 'create', 'b1/test', '1G', '2G', '3G'
      
      backend.commands.should =~ [
        "zfs create -o #{QemuToolkit::EXPORT_TAG}=false b1/test",
        'zfs create -V 1G b1/test/disk1',
        'zfs create -V 2G b1/test/disk2',
        'zfs create -V 3G b1/test/disk3',
      ]
    end 
    it "fails for relative storage names (must specify a root)" do
      expect {
        invoke 'create', 'test', '1G'
      }.to raise_error
    end 
  end
end