require 'spec_helper'

require 'qemu-toolkit'
require 'qemu-toolkit/vmadm'

require 'support/backend_fake'

describe 'bin/vmadm' do
  let(:backend) { BackendFake.new(/dladm/ => '') }

  def invoke(*given_args)
    flexmock(QemuToolkit::Config.current, backend: backend)
    
    given_args.insert 1, '--vmpath', fixture_path('etc')
    given_args.insert 1, '--varrun', fixture_path('varrun')
    
    storadm = QemuToolkit::Vmadm.new('vmadm', context={})
    storadm.run given_args
  end
  def stdout(*args)
    capture(:stdout) {
      invoke(*args)
    }
  end
  
  describe 'list' do
    it "lists all vms" do
      stdout('list').should include('vm1')
    end 
    it "contains multiple vms in one file" do
      list = stdout('list')
      
      list.should include('multi1')
      list.should include('multi2')
    end 
  end
  describe 'create VM' do
    let(:config_file) { fixture_path('etc/foobar.rb') }
    let(:varrun_dir)  { fixture_path('varrun/foobar') }
    before(:each) { 
      FileUtils.mkdir_p fixture_path('varrun') }
    
    # Remove config_file
    before(:each) { 
      FileUtils.rm_f(config_file) if File.exist?(config_file) }
    before(:each) { 
      FileUtils.rm_rf(varrun_dir) if File.directory?(varrun_dir) }
      
    before(:each) { 
      backend.mapping.store(/zfs list -oname,#{QemuToolkit::EXPORT_TAG} -H/, '') }
    
    it "creates a configuration file template" do
      invoke 'create', 'foobar'
      
      Pathname.new(config_file).should be_file
    end 
    context "when local pools contain a corresponding storage space" do
      before(:each) { 
        backend.mapping.store(
          /zfs list -oname,#{QemuToolkit::EXPORT_TAG} -H/, 
          %q(
            pool1      -
            pool1/foobar    true
            pool1/foobar/disk1      true
            pool1/foobar/disk2      true
          )) }
      
      it "links the template to local zvols if possible" do
        invoke 'create', 'foobar'

        File.readlines(config_file).map(&:strip).should include(
          "disk '/dev/zvol/rdsk/pool1/foobar/disk1'")
        File.readlines(config_file).map(&:strip).should include(
          "disk '/dev/zvol/rdsk/pool1/foobar/disk2'")
      end 
    end
    it "fails if the machine exists" do
      invoke 'create', 'foobar'
      
      expect {
        invoke('create', 'foobar')
      }.to raise_error(SystemExit)
    end
    it "creates /var/run subdirectory for running the vm" do
      invoke 'create', 'foobar'

      Pathname.new(varrun_dir).should be_directory
    end 
  end
  describe 'start VM' do
    it "starts a virtual machine" do
      stdout('-n', 'start', 'vm1').should match(/-name vm1/)
    end 
    it "errors out cleanly if the machine doesn't exist" do
      begin
        invoke('-n', 'start', 'doesntexist')
      rescue SystemExit
      end
    end 
  end
  describe 'with mocked vm' do
    let(:vm) { flexmock('vm') }
    before(:each) { 
      flexmock(QemuToolkit::VM).
        should_receive(:[] => vm) }
    
    describe 'monitor VM' do
      it "executes a socat tunnel to the monitor socket" do
        vm.should_receive(:connect).with(:monitor).once
        invoke 'monitor', 'vm1'
      end 
    end
    describe 'kill VM' do
      it "runs VM#kill" do
        vm.should_receive(:kill).once

        invoke('kill', 'vm1')
      end 
    end
    describe 'shutdown VM' do
      it "runs VM#shutdown" do
        vm.should_receive(:shutdown).once
        
        invoke('shutdown', 'vm1')
      end 
    end
  end
end