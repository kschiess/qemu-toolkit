require 'qemu-toolkit'
require 'tmpdir'

$rspec_executing = true

def project_path(*args)
  File.expand_path(
    File.join(
      File.dirname(__FILE__), 
      '..', 
      *args))
end
def fixture_path(*args)
  File.expand_path(
    File.join(
      File.dirname(__FILE__), 
      'fixture', 
      *args))
end

def capture(stream)
  begin
    stream = stream.to_s
    eval "$#{stream} = StringIO.new"
    yield
    result = eval("$#{stream}").string
  ensure
    eval("$#{stream} = #{stream.upcase}")
  end

  result
end

RSpec.configure do |config|
  config.mock_with :flexmock
  
  config.before(:each) {
    QemuToolkit::Config.reset
    
    QemuToolkit::Config.etc = fixture_path('etc')
    QemuToolkit::Config.var_run = fixture_path
  }
end