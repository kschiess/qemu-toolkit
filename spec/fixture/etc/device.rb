virtual_machine "device" do
  disk '/dev/null'

  device 'driver', a: 'test', b: 'test', c_d: 'test', x_txtimer: 'foo', x_txburst: 'bar'
end