virtual_machine "iscsi" do
  disk '/dev/null'

  iscsi_target 'iqn.2010-01.com.qemu-toolkit:vm1', "10.0.30.1"
end