virtual_machine "nic" do
  disk '/dev/null'
  
  # This is a shortcut to create a connection to a vnic. 
  nic 'eth0', 
    macaddr: '2:8:20:52:a6:7e', 
    model: 'virtio', 
    via: 'igb1'

  # This is what happens below the covers.
  net :vnic, vlan: 2, name: 'vm1', ifname: 'vm1', macaddr: '1:8:20:52:a6:7e'
  net :nic, vlan: 2, name: "vm1", model: "virtio", macaddr: '1:8:20:52:a6:7e'

  # And this demonstrates how to configure the built-in DHCP-server: 
  nic 'eth2', 
    macaddr: '0:24:81:67:93:99', model: 'e1000g',
    via: 'igb2', 
    # Important part follows: 
    ip: '10.0.0.2', netmask: '255.255.255.0', 
    gateway_ip: '10.0.0.1', hostname: 'foobar', dns_ip: '8.8.8.8,8.8.4.4'
end