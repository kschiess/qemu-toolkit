require 'spec_helper'

require 'support/backend_fake'

describe QemuToolkit::VMStorage do
  let(:backend) { BackendFake.new }
  let(:vm_storage) { described_class.new('pool1/foo', backend) }
  
  describe '#exported?' do
    it "returns true if qemu-toolkit:export is set" do
      backend.mapping.store %r(^zfs get.*), "true\n"
      
      vm_storage.should be_exported
    end 
    it "returns false otherwise" do
      flexmock(backend).
        should_receive(:zfs).and_raise("...")
      vm_storage.should_not be_exported
    end 
  end
end