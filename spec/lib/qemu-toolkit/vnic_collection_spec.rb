require 'spec_helper'

describe QemuToolkit::VnicCollection do
  let(:coll) { described_class.new }

  describe "allocate" do
    let(:vnic) { flexmock(macaddr: '01:02:0a:04:05:06', over: 'igb0') }
    before(:each) { coll << vnic }
    
    it "filters for normalized mac address" do
      coll.allocate('igb0', '01:02:0a:4:5:6').should == vnic
    end
  end
end