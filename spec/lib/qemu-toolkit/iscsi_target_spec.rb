require 'spec_helper'

require 'qemu-toolkit/iscsi_target'

require 'support/backend_fake'

describe QemuToolkit::ISCSITarget do
  let(:backend) { BackendFake.new(
    /^iscsiadm list target/ => File.read(
      fixture_path('command_output/iscsiadm_list_target_1.txt'))) }
  let(:target) { described_class.new('iqn', 'address', backend) }
  
  describe '#disks' do
    it "configures the target if it doesn't exist" do
      flexmock(target).
        should_receive(:mapped?).and_return(false, true).
        should_receive(:puts).should_receive(:print)
      target.ensure_exists
      
      backend.commands.should == [
        "iscsiadm add static-config iqn,address 2>/dev/null"]
    end
    it "returns all disks, converted to real devices" do
      flexmock(target, :iqn => 'iqn.2012-01.com.qemu-toolkit:foo')
      
      target.disks.should == [
        '/dev/rdsk/c4t600144F0503CC9000000503F8B090009d0p0',
        '/dev/rdsk/c4t600144F0503CC9000000503F8B09000Ad0p0'] 
        
      backend.commands.should == ["iscsiadm list target -vS"]
    end
  end
end