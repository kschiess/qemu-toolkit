class BackendFake < QemuToolkit::Backend::Illumos
  def initialize(mapping={})
    @mapping = mapping
    @commands = []
  end
  
  attr_reader :mapping
  
  def run_cmd(*args)
    cmd = args.join(' ')
    @commands << cmd

    @mapping.each do |re, text|
      return text if re.match(cmd)
    end
    return text if text=@mapping[args.first]
  end
  
  # Simulate two disks (output of 
  # 'zfs list -H -o name -t volume -r b1/dns1')
  def disks(name)
    %w(disk1 disk2).map do |disk_name|
      [name, disk_name].join('/')
    end
  end
  
  attr_reader :commands
end
